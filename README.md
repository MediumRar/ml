# Minimal Launcher
Yes. Another Application Launcher. Why? On my search for the most ideal programs, for my "perfect" desktop enironment, I got really frustrated how a lot of projects handle things. Especially Window Managers, Bars and Application Launchers. They are either too minimal (suckless) or too complex (Qtile, Rofi etc.). Don't get me wrong, suckless is doing great and all, but I don't have an infinete amount of time to adapt their applications to my needs.

So yeah... Here I am starting my own projects I guess. A minimal application launcher utilizing the QT framework and written in C++.

## Goals
- Nothing but an Application Launcher
- Easy to use
- Minimal
- Configurable
- Customizable
- Full control via Keyboard
- Target OS -> Linux

## Planned Features
- Shortcut to pass command line arguments
- Config
  - $PATH
  - Keys
  - Style

## Install
Install your OSs qt6-base package and then:
```
git clone https://gitlab.com/MediumRar/ml
cd ml
qmake
make
```

Execute and test with:
```
cd tgt
./ml
```

## Configure
To change which paths ml shall search, set ```ML_PATH``` variable. Otherwise ml will search in ```PATH```.

All other configs can be found in ml/cfg/ml.conf. ml will search for this config in ```XDG_CONFIG_HOME/ml/ml.conf``` and fall back to default values if it can't find it.

## Usage
| Command | Description |
| - | - |
| ```Escape``` | Close |
| ```Tab``` | Autocomplete |
| ```Return``` | Execute selected application and close ml |

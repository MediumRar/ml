#include <QApplication>
#include "../inc/App.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    App my_app;
    my_app.show();
    return app.exec();
}

#include "../inc/Selection.h"

Selection::Selection(QWidget* parent) : QListWidget(parent) {
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFocusPolicy(Qt::NoFocus);

    setFont(QFont(
        Config::getOrDefault("font", QString("Arial")),
        Config::getOrDefault("font_size", 20)));
    setStyleSheet(R"(
        QListWidget::item {
            color: #999999;  /* Default text color */
            background-color: rgba(0, 0, 0, 0);
        }
        QListWidget::item:selected {
            color: #FFFFFF;
        }
        QListWidget::item:selected:!active {
            color: #FFFFFF;
        }
        QListWidget::item:selected:active {
            color: #FFFFFF;
        }
    )");
}

void Selection::update(const QRegExp& regex) {
    QStringList files = FileUtil::getFiles();
    QStringList filtered = files.filter(regex);
    filtered.sort();
    filtered = filtered.mid(0, 10);
    clear();
    addItems(filtered);
    setCurrentRow(0);
    int total_height = 0;
    for (int i = 0; i < count(); ++i) {
        total_height += sizeHintForRow(i);
    }
    setMaximumHeight(total_height);
}

void Selection::launchSelected() {
    if (!currentItem()) return;
    QProcess::startDetached(currentItem()->text(), {});
    qApp->quit();
}

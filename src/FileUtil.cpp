#include "../inc/FileUtil.h"

QStringList FileUtil::files;
std::mutex FileUtil::filesMutex;

QStringList FileUtil::getFiles() {
    std::lock_guard<std::mutex> lock(filesMutex);
    if (!files.isEmpty()) return files;

    const char* envVal;
    envVal = std::getenv("ML_PATH");
    if (envVal == nullptr) envVal = std::getenv("PATH");
    if (envVal == nullptr) return QStringList();

    files.clear();
    QStringList paths = QString::fromLocal8Bit(envVal).split(':');

    for (const QString& currentPath : paths) {
        QDir currentDir(currentPath);
        QStringList filesInCurrentDir = currentDir.entryList(QDir::Files);
        for (const QString& file : filesInCurrentDir) {
            if (!files.contains(file)) files.append(file);
        }
    }

    return files;
}

QString FileUtil::getCommonFilePrefix(const QString& minCommon) {
    if (minCommon.isEmpty()) return QString();

    QStringList matchingFiles;
    for (const QString& filename : getFiles()) {
        if (filename.contains(QRegExp("^" + minCommon))) matchingFiles.append(filename);
    }

    QString commonPrefix;
    if (!matchingFiles.isEmpty()) {
        commonPrefix = matchingFiles.first();
        for (const QString& filename : matchingFiles) {
            int len = 0;
            while (len < commonPrefix.length() && len < filename.length() && commonPrefix.at(len) == filename.at(len)) {
                len++;
            }
            commonPrefix = commonPrefix.left(len);
        }
    }

    return commonPrefix;
}

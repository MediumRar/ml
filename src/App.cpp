#include "../inc/App.h"

App::App(QWidget* parent) : QWidget(parent) {
    setWindowTitle("Minimal Launcher");
    /*
     * Instead of using
     *  setWindowState(Qt::WindowFullScreen);
     *  or
     *  setWindowState(Qt::WindowMaximized);
     * Avoid weird bugs by calling below:
     *  QTimer::singleShot(0, this, SLOT(showFullScreen()));
     */
    QTimer::singleShot(0, this, SLOT(showFullScreen()));
    setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::Tool);
    setAttribute(Qt::WA_TranslucentBackground);

    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->setAlignment(Qt::AlignCenter);

    MasterWrapper* input_wrapper = new MasterWrapper(this);
    layout->addWidget(input_wrapper);
}

void App::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Escape) {
        qApp->quit();
    }
}

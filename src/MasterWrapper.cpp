#include "../inc/MasterWrapper.h"

MasterWrapper::MasterWrapper(QWidget* parent) : QLabel(parent) {
    setStyleSheet(R"(
        * {
            background-color: rgba(0, 0, 0, 0.6);
            color: #FFFFFF;
        }
        MasterWrapper {
            background-color: rgba(0, 0, 0, 0.8);
        }
        ContentWrapper {
            background-color: rgba(0, 0, 0, 0);
        }
    )");
    
    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->setContentsMargins(0, 100, 0, 0);
    layout->setAlignment(Qt::AlignCenter);
    ContentWrapper* content = new ContentWrapper(this);
    layout->addWidget(content);
    layout->addStretch();
}

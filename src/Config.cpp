#include "../inc/Config.h"

std::unordered_map<std::string, std::string> Config::config;
std::mutex Config::configMutex;

std::string Config::getOrDefault(const std::string& key, const std::string& defaultValue) {
    std::lock_guard<std::mutex> lock(configMutex);
    const std::unordered_map<std::string, std::string>& config = load();

    auto it = config.find(key);
    return (it != config.end()) ? it->second : defaultValue;
}

QString Config::getOrDefault(const std::string& key, const QString& defaultValue) {
    std::string stringDefaultValue = defaultValue.toStdString();
    std::string stringValue = getOrDefault(key, stringDefaultValue);
    return QString::fromStdString(stringValue);
}

size_t Config::getOrDefault(const std::string& key, const std::size_t& defaultValue) {
    std::string stringDefaultValue = std::to_string(defaultValue);
    std::string stringValue = getOrDefault(key, stringDefaultValue);

    size_t value = defaultValue;
    try {
        value = std::stoull(stringValue);
    } catch (const std::exception& e) {
        std::cerr << "Error converting string to std::size_t: " << e.what() << std::endl;
    }
    return value;
}

const std::unordered_map<std::string, std::string>& Config::load() {
    if (!config.empty()) return config;

    const char* envVal;
    envVal = std::getenv("XDG_CONFIG_HOME");
    if (envVal == nullptr) return config;

    std::string configFilepath = std::string(envVal) + "/ml/ml.conf";
    std::ifstream configFile(configFilepath);
    if (configFile.is_open()) {
        std::string line;
        while (std::getline(configFile, line)) {
            if (line.empty()) continue;
            if (line.find('#') != std::string::npos) continue;
            std::istringstream iss(line);
            std::string key, value;
            if (std::getline(iss, key, '=') && std::getline(iss, value)) {
                key = filter(key);
                value = filter(value);
                config[key] = value;
            }
        }
        configFile.close();
    } else std::cerr << "Unable to open file: " << configFilepath << std::endl;
    return config;
}

std::string Config::filter(const std::string& str) {
    std::string result;
    for (char c : str) {
        if (std::isalnum(c)
                || c == '.'
                || c == '('
                || c == ')'
                || c == '_') {
            result += c;
        }
    }
    return result;
}

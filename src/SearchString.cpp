#include "../inc/SearchString.h"

SearchString::SearchString(
        QWidget* parent,
        Selection* selection) :
            QLineEdit(parent),
            selection(selection) {
    setFont(QFont(
        Config::getOrDefault("font", QString("Arial")),
        Config::getOrDefault("font_size", 20)));

    selection->update(searchRegEx());
}

void SearchString::keyPressEvent(QKeyEvent *event) {
    QLineEdit::keyPressEvent(event);
    selection->update(searchRegEx());
    switch(event->key()) {
        case Qt::Key_Tab:
            update();
            break;
        case Qt::Key_Return:
            selection->launchSelected();
            break;
    }
}

QRegExp SearchString::searchRegEx() const {
    return QRegExp("^" + text());
}

void SearchString::update() {
    QString commonPrefix = FileUtil::getCommonFilePrefix(text());
    if (!commonPrefix.isEmpty()) setText(commonPrefix);
}

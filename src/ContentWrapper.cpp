#include "../inc/ContentWrapper.h"

ContentWrapper::ContentWrapper(QWidget* parent) : QLabel(parent) {
    setMaximumSize(600, 600);
    QFont font;
    font.setPointSize(20);

    layout = new QVBoxLayout(this);
    selection = new Selection(this);
    searchstring = new SearchString(this, selection);

    layout->setSpacing(20);
    layout->setAlignment(Qt::AlignTop);
    layout->addWidget(searchstring);
    layout->addWidget(selection);
}

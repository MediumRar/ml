#pragma once

#include <QLabel>
#include <QVBoxLayout>
#include "ContentWrapper.h"

class MasterWrapper : public QLabel {
    Q_OBJECT

public:
    MasterWrapper(QWidget* parent = nullptr);
};

#pragma once

#include <iostream>
#include <cctype>
#include <fstream>
#include <mutex>
#include <sstream>
#include <string>
#include <unordered_map>

#include <QString>

class Config {
public:
    static std::string getOrDefault(const std::string& key, const std::string& defaultValue);
    static QString getOrDefault(const std::string& key, const QString& defaultValue);
    static size_t getOrDefault(const std::string& key, const std::size_t& defaultValue);

private:
    static std::unordered_map<std::string, std::string> config;
    static std::mutex configMutex;

    static const std::unordered_map<std::string, std::string>& load();
    static std::string filter(const std::string& str);
};

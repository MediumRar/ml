#pragma once

#include <QApplication>
#include <QDir>
#include <QListWidget>
#include <QProcess>

#include "Config.h"
#include "FileUtil.h"

class Selection : public QListWidget {
    Q_OBJECT

public:
    Selection(QWidget* parent = nullptr);
    void update(const QRegExp& regex);
    void launchSelected();
};

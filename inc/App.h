#pragma once

#include <QApplication>
#include <QKeyEvent>
#include <QShortcut>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>

#include "MasterWrapper.h"

class App : public QWidget {
    Q_OBJECT

public:
    App(QWidget* parent = nullptr);
protected:
    void keyPressEvent(QKeyEvent *event) override;
};

#pragma once

#include <QDir>
#include <QLabel>
#include <QKeyEvent>
#include <QVBoxLayout>

#include "SearchString.h"
#include "Selection.h"

class ContentWrapper : public QLabel {
    Q_OBJECT

public:
    ContentWrapper(QWidget* parent = nullptr);

private:
    QVBoxLayout* layout;
    SearchString* searchstring;
    Selection* selection;
};

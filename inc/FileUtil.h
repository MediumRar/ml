#pragma once

#include <mutex>

#include <QString>
#include <QDir>
#include <QRegExp>
#include <QStringList>

class FileUtil {
public:
    static QStringList getFiles();
    static QString getCommonFilePrefix(const QString& minCommon);

private:
    static QStringList files;
    static std::mutex filesMutex;
};

#pragma once

#include <iostream>
#include <QKeyEvent>
#include <QLineEdit>

#include "Config.h"
#include "FileUtil.h"
#include "Selection.h"

class SearchString : public QLineEdit {
    Q_OBJECT

public:
    SearchString(QWidget* parent, Selection* selection);
    void keyPressEvent(QKeyEvent *event);
    void update();
    QRegExp searchRegEx() const;

private:
    Selection* selection;
};
